<?php

$host     = 'localhost';
$dbname   = 'test';
$user     = 'root';
$password = 'root';

try {
  $db = new PDO("mysql:host=$host;dbname=$dbname", $user, $password);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  $db->exec("set names utf8");
} catch (PDOException $e) {
  echo $e->getMessage();
}