<?php

require_once dirname(__FILE__) . '/db.php';

$id = trim($_GET['name']);
$id = strip_tags($id);

$stmt = $db->prepare("SELECT value  FROM w4p_values WHERE id=?");
$stmt->bindValue(1, $id, PDO::PARAM_INT);
$stmt->execute();
$row    = $stmt->fetchAll(PDO::FETCH_ASSOC);
$string = $row[0]['value'];

if ($string) {
  $image = ImageCreateFromPNG("image-" . $id . ".png") or die ("Cannot Initialize new GD image stream");
  $color      = imagecolorallocate($image, 255, 0, 0);
  $text_color = imagecolorallocate($image, 233, 14, 91);

  imagestring($image, 5, 10, 150, $string, $color);

  header('content-type: image/png');
  imagepng($image);

  imagedestroy($image);

  $stmt = $db->prepare("UPDATE w4p_values set count = count+1 where id=:id");
  $stmt->bindParam(':id', $id);
  $stmt->execute();

}
else {
  echo "IMAGE NOT FOUND";
}